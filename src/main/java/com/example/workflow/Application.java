package com.example.workflow;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
@SpringBootApplication
@RequiredArgsConstructor
public class Application {
    private final IdentityService identityService;
    private final RuntimeService runtimeService;

    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }

    @GetMapping("/start/{processKey}")
    public void start(@PathVariable(value = "processKey") String processKey) {
//    identityService.setAuthenticatedUserId("xiaoming");
//    VariableMap variables = Variables.createVariables();
//    variables.put("free", false);
//    runtimeService.startProcessInstanceByKey(processKey, variables);


        // 多实例任务
//    identityService.setAuthenticatedUserId("xiaoming");
//    VariableMap variables = Variables.createVariables();
//    List<String> leaders = new LinkedList<>();
//    leaders.add("wangbing");
//    leaders.add("zhangsan");
//    leaders.add("wangwu");
//    variables.put("leaders", leaders);
//    runtimeService.startProcessInstanceByKey(processKey, variables);


        // 脚本任务
//    identityService.setAuthenticatedUserId("xiaoming");
//    VariableMap variables = Variables.createVariables();
//    variables.put("originDays", 10);
//    runtimeService.startProcessInstanceByKey(processKey, variables);

        VariableMap variables = Variables.createVariables();
        identityService.setAuthenticatedUserId("xiaoming");
        List<String> videoNames = new LinkedList<>();
        videoNames.add("电影");
        videoNames.add("电视剧");
        videoNames.add("综艺节目");
        variables.put("videoNames", videoNames);
        runtimeService.startProcessInstanceByKey(processKey, variables);

    }

    @GetMapping("/executions/{processInstanceId}")
    public List<Execution> getAllExecutions(@PathVariable(value = "processInstanceId") String processInstanceId) {
        List<Execution> list = runtimeService.createExecutionQuery().processInstanceId(processInstanceId).list();
        return list;
    }

}