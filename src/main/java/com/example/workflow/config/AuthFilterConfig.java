package com.example.workflow.config;

import jakarta.servlet.FilterRegistration;
import jakarta.servlet.ServletContext;
import org.camunda.bpm.engine.rest.security.auth.ProcessEngineAuthenticationFilter;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Configuration;

/**
 * @author 侯宁 on 2024/4/25
 */
@Configuration
public class AuthFilterConfig implements ServletContextInitializer {

    @Override
    public void onStartup(ServletContext servletContext) {
        FilterRegistration.Dynamic authFilter = servletContext.addFilter(
                "camunda-auth", ProcessEngineAuthenticationFilter.class);
        authFilter.setAsyncSupported(true);
        authFilter.setInitParameter("authentication-provider", "org.camunda.bpm.engine.rest.security.auth.impl.HttpBasicAuthenticationProvider");
        authFilter.addMappingForUrlPatterns(null, true, "/engine-rest/*");
    }
}
