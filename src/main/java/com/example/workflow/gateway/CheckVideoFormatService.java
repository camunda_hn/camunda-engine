package com.example.workflow.gateway;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/**
 * @author 侯宁 on 2024/5/4
 */
@Slf4j
@Component("checkVideoFormat")
public class CheckVideoFormatService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {
        log.info("进入检测视频格式任务");
        Object videoName = execution.getVariable("targetVideoName");
        log.info("视频名称: {}", videoName);
    }
}
