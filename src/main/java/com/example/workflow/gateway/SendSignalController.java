package com.example.workflow.gateway;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 侯宁 on 2024/5/5
 */
@RestController
@RequiredArgsConstructor
public class SendSignalController {

    private final RuntimeService runtimeService;

    @GetMapping("/sendSignal")
    public boolean sendSignal() {
        runtimeService.createSignalEvent("Signal_direct_leader").send();
        return true;
    }
}
