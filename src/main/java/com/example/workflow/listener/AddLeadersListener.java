package com.example.workflow.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 侯宁 on 2024/5/3
 */
@Slf4j
@Component("addLeaders")
public class AddLeadersListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution execution) {
        long leaveDay = (long) execution.getVariable("leaveDays");
        log.info("进入增加领导集合类，员工请假天数：{}", leaveDay);
        List<String> leaders = new ArrayList<>();
        if (leaveDay > 3 && leaveDay <= 5) {
            leaders.add("wangbing");
            leaders.add("zhangsan");
        } else if (leaveDay > 5) {
            leaders.add("wangbing");
            leaders.add("zhangsan");
            leaders.add("wangwu");
        }
        execution.setVariable("leaders", leaders);
    }
}
