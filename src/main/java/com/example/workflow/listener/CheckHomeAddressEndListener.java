package com.example.workflow.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;

/**
 * @author 侯宁 on 2024/4/25
 */
@Slf4j
@Component("checkHomeAddress")
public class CheckHomeAddressEndListener implements ExecutionListener {
    @SuppressWarnings("FieldCanBeLocal")
    private final String DEFAULT_ADDRESS = "用户注册地址";

    @Override
    public void notify(DelegateExecution execution) {
        log.info("进入检查用户地址任务");
        Object homeAddress = execution.getVariable("homeAddress");
        if (homeAddress == null) {
            log.info("用户没填地址，使用默认地址");
            execution.setVariable("homeAddress", DEFAULT_ADDRESS);
        }
    }
}
