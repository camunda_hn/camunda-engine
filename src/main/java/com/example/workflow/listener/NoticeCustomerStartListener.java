package com.example.workflow.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author 侯宁 on 2024/4/25
 */
@Slf4j
@Component("noticeCustomer")
public class NoticeCustomerStartListener implements ExecutionListener {

    @Override
    public void notify(DelegateExecution execution) throws Exception {
        log.info("进入通知客户任务");
        String homeAddress = String.valueOf(execution.getVariable("homeAddress"));
        TimeUnit.SECONDS.sleep(1);

        log.info("您好，师傅正在赶往{}为您修理家电", homeAddress);
    }
}
