package com.example.workflow.message.event;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/**
 * @author 侯宁 on 2024/5/6
 */
@Slf4j
@Component("handleOrder")
public class HandleOrderService implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        log.info("处理订餐订单");
        String endpoint = (String) execution.getVariable("endpoint");
        RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();
        if ("alipay".equals(endpoint)) {
//            runtimeService.startProcessInstanceByMessage("Message_alipay");
            runtimeService.startProcessInstanceByMessage("Message_alipay_lane");
        } else if ("wechat".equals(endpoint)) {
//            runtimeService.startProcessInstanceByMessage("Message_wechat");
            runtimeService.startProcessInstanceByMessage("Message_wechat_lane");
        }
    }
}
