package com.example.workflow.script.task;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * @author 侯宁 on 2024/4/26
 */
@Slf4j
@Service("queryAnnualLeave")
public class QueryAnnualLeaveService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {
        log.info("进入查询剩余年假任务");
        int leftAnnualDays = (int) execution.getVariable("leftAnnualDays");
        log.info("剩余年假天数: {}", leftAnnualDays);
    }
}
