package com.example.workflow.send.task;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/**
 * 发送任务
 *
 * @author 侯宁 on 2024/5/3
 */
@Slf4j
@Component("sendTask")
public class SendTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        log.info("sendTask发送消息");
        RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();
        runtimeService.createMessageCorrelation("Message_receive_task-test")
                .processInstanceBusinessKey("message_businessKey")
                .correlate();
    }
}
