package com.example.workflow.service.task;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * 师傅上门修理
 *
 * @author 侯宁 on 2024/4/25
 */
@Slf4j
@Service("doRepair")
public class DoingRepairService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {
        log.info("开始上门修理");
        String currentActivityName = execution.getCurrentActivityName();
        log.info("当前活动名: {}", currentActivityName);
        execution.setVariable("repairManName", "王小满");
    }
}
