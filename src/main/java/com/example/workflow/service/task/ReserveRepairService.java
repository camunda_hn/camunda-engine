package com.example.workflow.service.task;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * 预约修理家电
 *
 * @author 侯宁 on 2024/4/25
 */
@Slf4j
public class ReserveRepairService implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        log.info("进入到预约家电修理任务");
        String currentActivityName = execution.getCurrentActivityName();
        // 预约的具体调用
        // ...
        String processDefinitionId = execution.getProcessDefinitionId();
        log.info("当前活动名称: {} 流程定义id: {}", currentActivityName, processDefinitionId);
    }
}
